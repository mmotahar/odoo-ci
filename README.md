Dockerized Odoo for CI
======================

This Docker Image contains all the things needed for running unit tests 
on bitbucket pipelines or with gitlab runners.

This image is as small as possible but has installed the full postgresql
server and the odoo instance.



